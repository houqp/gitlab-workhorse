/*
The upstream type implements http.Handler.

In this file we handle request routing and interaction with the authBackend.
*/

package upstream

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/gitlab-org/gitlab-workhorse/internal/badgateway"
	"gitlab.com/gitlab-org/gitlab-workhorse/internal/helper"
	"gitlab.com/gitlab-org/gitlab-workhorse/internal/urlprefix"
)

var DefaultBackend = helper.URLMustParse("http://localhost:8080")

type Upstream struct {
	Backend         *url.URL
	Version         string
	SecretPath      string
	DocumentRoot    string
	DevelopmentMode bool

	URLPrefix    urlprefix.Prefix
	Routes       []route
	RoutesMap    map[string][]*route
	RoundTripper *badgateway.RoundTripper
}

func NewUpstream(backend *url.URL, socket, version, secretFile, documentRoot string, developmentMode bool, proxyHeadersTimeout time.Duration) *Upstream {
	up := Upstream{
		Backend:         backend,
		Version:         version,
		SecretPath:      secretFile,
		DocumentRoot:    documentRoot,
		DevelopmentMode: developmentMode,
	}
	if backend == nil {
		up.Backend = DefaultBackend
	}
	up.RoundTripper = badgateway.NewRoundTripper(up.Backend, socket, proxyHeadersTimeout)
	up.configureURLPrefix()
	up.configureRoutes()
	return &up
}

func (u *Upstream) configureURLPrefix() {
	relativeURLRoot := u.Backend.Path
	if !strings.HasSuffix(relativeURLRoot, "/") {
		relativeURLRoot += "/"
	}
	u.URLPrefix = urlprefix.Prefix(relativeURLRoot)
}

func (u *Upstream) matchRoute(method string, uripath string) *route {
	var ro *route
	stripped_uri := u.URLPrefix.Strip(uripath)
	for _, ro = range u.RoutesMap[method] {
		if ro.regex == nil || ro.regex.MatchString(stripped_uri) {
			return ro
		}
	}
	for _, ro = range u.RoutesMap[""] {
		if ro.regex == nil || ro.regex.MatchString(stripped_uri) {
			return ro
		}
	}
	return nil
}

func (u *Upstream) ServeHTTP(ow http.ResponseWriter, r *http.Request) {
	w := helper.NewLoggingResponseWriter(ow)
	defer w.Log(r)

	// Drop WebSocket connection and CONNECT method
	if r.RequestURI == "*" {
		helper.HTTPError(&w, r, "Connection upgrade not allowed", http.StatusBadRequest)
		return
	}

	// Disallow connect
	if r.Method == "CONNECT" {
		helper.HTTPError(&w, r, "CONNECT not allowed", http.StatusBadRequest)
		return
	}

	// Check URL Root
	URIPath := urlprefix.CleanURIPath(r.URL.Path)
	if !u.URLPrefix.Match(URIPath) {
		helper.HTTPError(&w, r, fmt.Sprintf("Not found %q", URIPath), http.StatusNotFound)
		return
	}

	// Look for a matching Git service
	var ro *route = u.matchRoute(r.Method, URIPath)
	if ro != nil {
		ro.handler.ServeHTTP(&w, r)
	} else {
		// The protocol spec in git/Documentation/technical/http-protocol.txt
		// says we must return 403 if no matching service is found.
		helper.HTTPError(&w, r, "Forbidden", http.StatusForbidden)
	}
}
