package upstream

import (
	"io/ioutil"
	"net/url"
	"os"
	"testing"
	"time"
)

func getMockUpstream(secretFile *os.File) *Upstream {
	backendURL, _ := url.Parse(DefaultBackend.String())
	return NewUpstream(
		backendURL,
		"",
		"0.1",
		secretFile.Name(),
		"/tmp",
		false,
		5*time.Minute,
	)
}

func BenchmarkRouteMatching(b *testing.B) {
	secretFile, _ := ioutil.TempFile(os.TempDir(), "prefix")
	defer os.Remove(secretFile.Name())

	u := getMockUpstream(secretFile)
	for n := 0; n < b.N; n++ {
		u.matchRoute("GET", "/foo")
		u.matchRoute("POST", "/ci/api/v1/builds/123/artifacts")
		u.matchRoute("GET", "/foo/bar.git/info/refs")
		u.matchRoute("POST", "/foo/bar.git/git-upload-pack")
		u.matchRoute("POST", "/foo/bar.git/git-receive-pack")
		u.matchRoute("PUT", "/foo/bar.git/gitlab-lfs/objects/06632c751d46de97776f1a5aeb2432fae0f6fd4d/1234")
		u.matchRoute("GET", "/assets/foo")
		u.matchRoute("GET", "/uploads/foo")
	}
}
